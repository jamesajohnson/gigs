import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare const Buffer;

@Injectable()
export class SpotifyProvider {

  baseUrl = 'https://api.spotify.com/v1/';
  authUrl = 'https://accounts.spotify.com/api/token';

  client = {
    client_id: 'ed9061d60f2c44fab5800410ac5c9a8b', // Your client id
    client_secret: 'a8afef0297834c0fba9a8547ccdbed51', // Your secret
    redirect_uri: 'https://yourgiglist.com/spotify-callback/', // Your redirect uri
    scopes: 'user-read-private',
    response_type: 'token'
  }
  authOptions = {
    headers: {
      'Authorization': 'Basic ' + (new Buffer(this.client.client_id + ':' + this.client.client_secret).toString('base64'))
    },
    form: {
      grant_type: 'client_credentials'
    },
    json: true
  };

  constructor(public http: Http) {
    console.log('Hello SpotifyProvider Provider');
  }

  getAlbum(albumId: string): Promise<Object> {
    return this.http.get(this.spotify('albums/' + albumId))
    .toPromise()
    .then(response => response.json() as Object)
    .catch(this.handleError);
  }

  getUser(userId: string): Promise<Object> {
    let headers = new Headers({ 'Authorization': 'Basic ' + (new Buffer(this.client.client_id + ':' + this.client.client_secret).toString('base64')) });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.spotify('user/' + userId), options)
    .toPromise()
    .then(response => response.json() as Object)
    .catch(this.handleError);
  }

  handleError(): void {
    console.log('Error in Spotify provider');
  }

  getAuthUrl(): string {
    let params = new URLSearchParams();
    for(let key in this.client){
        params.set(key, this.client[key])
    }
    return 'https://accounts.spotify.com/authorize?' + params.toString();
  }

  spotify(endPoint: string): string {
    return this.baseUrl + endPoint;
  }
}
