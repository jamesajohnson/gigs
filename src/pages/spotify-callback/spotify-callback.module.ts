import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpotifyCallbackPage } from './spotify-callback';

@NgModule({
  declarations: [
    SpotifyCallbackPage,
  ],
  imports: [
    IonicPageModule.forChild(SpotifyCallbackPage),
  ],
  exports: [
    SpotifyCallbackPage
  ]
})
export class SpotifyCallbackPageModule {}
