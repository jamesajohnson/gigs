import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DomSanitizer } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { Deeplinks } from '@ionic-native/deeplinks';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpotifyProvider } from '../providers/spotify/spotify';

import { TabsPage } from '../pages/tabs/tabs';
import { SpotifyCallbackPage } from '../pages/spotify-callback/spotify-callback';

@Component({
    templateUrl: 'app.html',
    providers: [
        SpotifyProvider
    ]
})
export class MyApp {
    rootPage:any = TabsPage;

    userAuth: boolean = false
    authUrl: string;
    spotify: SpotifyProvider;
    sanitizer: DomSanitizer;

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, spotify: SpotifyProvider, inAppBrowser: InAppBrowser, deeplinks: Deeplinks) {
        this.spotify = spotify;
        this.authUrl = spotify.getAuthUrl();

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();

            deeplinks.route({
                'https://yourgiglist.com/spotify-callback/': SpotifyCallbackPage
            }).subscribe((match) => {
                // match.$route - the route we matched, which is the matched entry from the arguments to route()
                // match.$args - the args passed in the link
                // match.$link - the full link data
                console.log('Successfully matched route', match);
            }, (nomatch) => {
                // nomatch.$link - the full link data
                console.error('Got a deeplink that didn\'t match', nomatch);
            });

            inAppBrowser.create(this.authUrl);
        });
    }

    ngOnInit(): void {
        this.spotify.getUser('jjjamesjjj').then(function (data) {
            console.log(data);
        });
    }
}
